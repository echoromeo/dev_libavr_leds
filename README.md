# dev_libavr_leds

dev project that simplifies development of the the libavr neopixel and dotstar libraries as submodules and collect them in one Atmel Studio solution

For AVR DA CNANO, the config has the data pin on PA4 and for dotstar SCK is on PA6

For ATtiny3217 Xplained Pro the example configs are the following:
* SPI: data pin on PC2 and for dotstar SCK is on PC0
* USART:data pin on PB2 and for dotstar SCK is on PB1