/*
 * main.c - remember this is not the submodule example!
 *
 * Created: 22.03.2020
 * Author: echoromeo
 */
#include <avr/io.h>
#include <util/delay.h>
#include <neopixel.h>


// Just an array with 10 different colors
static color_t data[] = {
	{.r = 0xff, .g = 0, .b = 0},
	{.r = 0, .g = 0xff, .b = 0},
	{.r = 0, .g = 0, .b = 0xff},
	{.r = 0, .g = 0x7f, .b = 0x7f},
	{.r = 0x7f, .g = 0, .b = 0x7f},
	{.r = 0x7f, .g = 0x7f, .b = 0},
	{.r = 0x55, .g = 0x55, .b = 0x55},
	{.r = 0x80, .g = 0x40, .b = 0x40},
	{.r = 0x40, .g = 0x80, .b = 0x40},
	{.r = 0x40, .g = 0x40, .b = 0x80},
};

int main(void)
{
	// Configure clock
	_PROTECTED_WRITE(CLKCTRL.OSCHFCTRLA, CLKCTRL_FRQSEL_24M_gc);
	
	// Init the driver
	neopixel_init();

	// Send the array to 10 LEDs
	neopixel_configure_array(data, sizeof(data)/sizeof(data[0]));
	_delay_ms(3000);
	
	// Turn off all but one red LED that moves
	uint8_t loop = 0;
	while (1)
	{

		neopixel_configure_constant_and_single((color_t) {.r=0x7f}, loop++, (color_t) {0}, sizeof(data)/sizeof(data[0]));

		if (loop >= sizeof(data)/sizeof(data[0]))
		{
			loop = 0;
		}

		_delay_ms(100);
	}
}
