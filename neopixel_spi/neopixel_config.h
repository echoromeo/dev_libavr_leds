/*
 * neopixel_config.h - remember this is not the submodule config!
 *
 * Created: 22.03.2020
 * Author: echoromeo
 */
#ifndef NEOPIXEL_CONFIG_H_
#define NEOPIXEL_CONFIG_H_

/*
 * Needed for the instance defines
 */
#include <avr/io.h>

/*
 * Select one Neopixel type to configure the driver correctly
 */
#define NEOPIXEL_TYPE_RGB
//	#define NEOPIXEL_TYPE_WWA
//	#define NEOPIXEL_TYPE_RGBW

/*
 * Configure the SPI instance and output pin to be used by the driver
 */
#define LED_SPI				SPI0
#define LED_SPI_PORT		PORTA
#define LED_DATA_PIN        PIN4_bm
#define LED_DATA_PINCTRL	PIN4CTRL

/*
 * Redefine one of these to move the SPI output to an alternate pinout
 */
#define LED_SPI_PORT_ALT				0 // Use this define for default pinout
//#define LED_SPI_PORT_ALT				PORTMUX_SPI0_ALT1_gc
//#define LED_SPI_PORT_ALT				PORTMUX_SPI0_ALT2_gc
//#define LED_SPI_PORT_ALT				PORTMUX_SPI0_ALTERNATE_gc //tiny-style
//#define LED_SPI_PORT_ALT				PORTMUX_SPI1_ALT1_gc
//#define LED_SPI_PORT_ALT				PORTMUX_SPI1_ALT2_gc

#endif /* NEOPIXEL_CONFIG_H_ */
