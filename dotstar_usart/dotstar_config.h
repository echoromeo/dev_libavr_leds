/*
 * dotstar_config.h - remember this is not the submodule config!
 *
 * Created: 22.03.2020
 * Author: echoromeo
 */
#ifndef DOTSTAR_CONFIG_H_
#define DOTSTAR_CONFIG_H_

/*
 * Needed for the instance defines
 */
#include <avr/io.h>

/*
 * Configure the USART instance and output pin to be used by the driver
 */
#define LED_USART			 USART0
#define LED_USART_PORT       PORTA
#define LED_DATA_PIN         PIN4_bm
#define LED_SCK_PIN          PIN6_bm

/*
 * Redefine one of these if you want to move the USART output to an alternate pinout
 * Check out the PORTMUX section in the device datasheet.
 */
//#define LED_USART_PORT_ALT      0 // Use this define for default pinout
#define LED_USART_PORT_ALT    PORTMUX_USART0_ALT1_gc
//#define LED_USART_PORT_ALT    PORTMUX_USART0_ALT2_gc 
//#define LED_USART_PORT_ALT    PORTMUX_USART1_ALT1_gc
//#define LED_USART_PORT_ALT    PORTMUX_USART1_ALT2_gc 
//#define LED_USART_PORT_ALT    PORTMUX_USART2_ALT1_gc
//#define LED_USART_PORT_ALT    PORTMUX_USART2_ALT2_gc 
//#define LED_USART_PORT_ALT    PORTMUX_USART3_ALT1_gc
//#define LED_USART_PORT_ALT    PORTMUX_USART3_ALT2_gc 
//#define LED_USART_PORT_ALT    PORTMUX_USART0_ALTERNATE_gc //tiny-style

/*
 * Uncomment one of these only if you want to move the USART4 or USART5 output to an alternate pinout
 * The driver will ignore LED_USART_PORT_ALT if one of these are defined 
 */

//#define LED_USART_PORT_ALTB			PORTMUX_USART4_ALT1_gc
//#define LED_USART_PORT_ALTB			PORTMUX_USART4_ALT2_gc 
//#define LED_USART_PORT_ALTB			PORTMUX_USART5_ALT1_gc
//#define LED_USART_PORT_ALTB			PORTMUX_USART5_ALT2_gc 

#endif /* DOTSTAR_CONFIG_H_ */
